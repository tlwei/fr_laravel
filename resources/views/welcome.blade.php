<!DOCTYPE html>
<html>
<title>FR</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
    .footer {
      left: 0;
      bottom: 0;
   }
   .header {
    position: fixed;
  }

  .img_style{
    width:100%;
    height:auto
  }
  @media only screen and (max-width: 480px) {
  .img_style{
    width:150%;
    height:auto
  }
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="header w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
      <a href="/" class="w3-bar-item w3-button"><b>FR</b> FinanceReport</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
        <a href="/" class="w3-bar-item w3-button">Projects</a>
        <a href="/" class="w3-bar-item w3-button">About</a>
        <a href="/" class="w3-bar-item w3-button">Contact</a>
    </div>
  </div>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
  <img style="img_style" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQlvR3uOgQnLFS4-Te-H8Bxz-G_qVKKX42WbeoB_aCJ40tZ7mRj6Q&s" alt="Architecture" width="1500" height="800">
  <div class="w3-display-middle w3-margin-top w3-center">
    <h1 class="w3-xxlarge w3-text-black"><span class="w3-padding w3-black w3-opacity-min"><b>FR</b></span> <span>FinanceReport</span></h1><br>
    <span class="">Upload your finance report now</span><br><p></p>
    <button type="button" class="btn btn-outline-dark btn-lg" data-toggle="modal" data-target="#pdf">TYPE PDF</button>&nbsp;
    <button type="button" class="btn btn-outline-dark btn-lg" data-toggle="modal" data-target="#img">TYPE IMAGE</button>
    
  </div>

</header>

<div class="modal fade" id="pdf" tabindex="-1" role="dialog" aria-labelledby="pdf" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="pdf">Upload PDF</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="/api/getFileRenew" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
              <input required name="title" type="title" class="form-control" id="inputEmail3" placeholder="Title">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">File</label>
            <div class="col-sm-10">
              <input accept="application/pdf" required name="file" type="file" class="form-control" id="inputEmail3" placeholder="file">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-12 col-form-label">Click to view supported finance report template (PDF)</label>
          </div>
          <a title="Template 1 PDF" href="/template/t1.pdf" target="_blank">
            <button type="button" class="col-sm-3 btn btn-outline-dark ">Template 1</button>
          </a>&nbsp;
          <a title="Template 2 PDF" href="/template/t2.pdf" target="_blank">
            <button type="button" class="col-sm-3 btn btn-outline-dark ">Template 2</button>
          </a>&nbsp;
          <a title="Template 3 PDF" href="/template/t3.pdf" target="_blank">
            <button type="button" class="col-sm-3 btn btn-outline-dark ">Template 3</button>
          </a>&nbsp;
          <p></p>
          
          <div class="modal-footer">
            <button type="submit" class="btn btn-lg btn-outline-dark ">Submit</button>
          </div>
        </form>
      </div>
     
    </div>
  </div>
</div>

<div class="modal fade" id="img" tabindex="-1" role="dialog" aria-labelledby="img" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="img">Upload Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="/api/getFileRenew" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
              <input required name="title" type="title" class="form-control" id="inputEmail3" placeholder="Title">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">File</label>
            <div class="col-sm-10">
              <input accept="image/*" required name="file" type="file" class="form-control" id="inputEmail3" placeholder="file">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-12 col-form-label">Click to view supported finance report template (Image)</label>
          </div>
          <a title="Template 1 IMG" href="/template/t1.jpg" target="_blank">
            <button type="button" class="col-sm-3 btn btn-outline-dark ">Template 1</button>
          </a>&nbsp;
          <a title="Template 2 IMG" href="/template/t2.jpg" target="_blank">
            <button type="button" class="col-sm-3 btn btn-outline-dark ">Template 2</button>
          </a>&nbsp;
          <a title="Template 3 PDF" href="/template/t3.jpg" target="_blank">
            <button type="button" class="col-sm-3 btn btn-outline-dark ">Template 3</button>
          </a>&nbsp;
          <p></p>
          <div class="modal-footer">
            <button type="submit" class="btn btn-lg btn-outline-dark ">Submit</button>
          </div>
        </form>
      </div>
     
    </div>
  </div>
</div>


{{--  <!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">

  <!-- Project Section -->
  <div class="w3-container w3-padding-32" id="projects">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">Projects</h3>
  </div>

  <div class="w3-row-padding">
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Summer House</div>
        <img src="/w3images/house5.jpg" alt="House" style="width:100%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Brick House</div>
        <img src="/w3images/house2.jpg" alt="House" style="width:100%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Renovated</div>
        <img src="/w3images/house3.jpg" alt="House" style="width:100%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Barn House</div>
        <img src="/w3images/house4.jpg" alt="House" style="width:100%">
      </div>
    </div>
  </div>

  <div class="w3-row-padding">
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Summer House</div>
        <img src="/w3images/house2.jpg" alt="House" style="width:99%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Brick House</div>
        <img src="/w3images/house5.jpg" alt="House" style="width:99%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Renovated</div>
        <img src="/w3images/house4.jpg" alt="House" style="width:99%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Barn House</div>
        <img src="/w3images/house3.jpg" alt="House" style="width:99%">
      </div>
    </div>
  </div>

  <!-- About Section -->
  <div class="w3-container w3-padding-32" id="about">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">About</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint
      occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
      laboris nisi ut aliquip ex ea commodo consequat.
    </p>
  </div>

  <div class="w3-row-padding w3-grayscale">
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="/w3images/team2.jpg" alt="John" style="width:100%">
      <h3>John Doe</h3>
      <p class="w3-opacity">CEO & Founder</p>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
      <p><button class="w3-button w3-light-grey w3-block">Contact</button></p>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="/w3images/team1.jpg" alt="Jane" style="width:100%">
      <h3>Jane Doe</h3>
      <p class="w3-opacity">Architect</p>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
      <p><button class="w3-button w3-light-grey w3-block">Contact</button></p>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="/w3images/team3.jpg" alt="Mike" style="width:100%">
      <h3>Mike Ross</h3>
      <p class="w3-opacity">Architect</p>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
      <p><button class="w3-button w3-light-grey w3-block">Contact</button></p>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="/w3images/team4.jpg" alt="Dan" style="width:100%">
      <h3>Dan Star</h3>
      <p class="w3-opacity">Architect</p>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
      <p><button class="w3-button w3-light-grey w3-block">Contact</button></p>
    </div>
  </div>

  <!-- Contact Section -->
  <div class="w3-container w3-padding-32" id="contact">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">Contact</h3>
    <p>Lets get in touch and talk about your next project.</p>
    <form action="/action_page.php" target="_blank">
      <input class="w3-input w3-border" type="text" placeholder="Name" required name="Name">
      <input class="w3-input w3-section w3-border" type="text" placeholder="Email" required name="Email">
      <input class="w3-input w3-section w3-border" type="text" placeholder="Subject" required name="Subject">
      <input class="w3-input w3-section w3-border" type="text" placeholder="Comment" required name="Comment">
      <button class="w3-button w3-black w3-section" type="submit">
        <i class="fa fa-paper-plane"></i> SEND MESSAGE
      </button>
    </form>
  </div>
  
<!-- Image of location/map -->
<div class="w3-container">
  <img src="/w3images/map.jpg" class="w3-image" style="width:100%">
</div>

<!-- End page content -->
</div>  --}}


<!-- Footer -->
<footer class="footer w3-center w3-black w3-padding-16">
  <p>Powered by <a href="/" title="Finance Report" target="_blank" class="w3-hover-text-green">Finance Report</a></p>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
