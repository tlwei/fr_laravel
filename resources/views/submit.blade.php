<!DOCTYPE html>
<html>
<title>FR</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
    .footer {
      left: 0;
      bottom: 0;
   }
   .header {
    position: fixed;
  }
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="header w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="/" class="w3-bar-item w3-button"><b>FR</b> FinanceReport</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="/" class="w3-bar-item w3-button">Projects</a>
      <a href="/" class="w3-bar-item w3-button">About</a>
      <a href="/" class="w3-bar-item w3-button">Contact</a>
    </div>
  </div>
</div>


<div style="padding:100px 10px 50px 10px" class="w3-display-container w3-content w3-wide" id="home">

    @if (isset($error) && $error = true)
      <text style="color:red">
        {{$return}}<br>
      </text>
    @else
      {{$return}}<br>
    @endif
      <br>
      @php
      if(isset($data)){
        echo "<pre> <p> <font color=blue>";
            print_r($data);
        echo "</pre> </font> </p>";
      }
      @endphp
      @if (isset($select))
      <form method="POST" action="/api/getFileRenew" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>
              <div class="col-sm-12">
                <input value={{$title}} required name="title" type="title" class="form-control" id="inputEmail3" placeholder="Title">
              </div>
            </div>

          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">File</label>
            <div class="col-sm-12">
              <input accept="image/*" value={{$file}} required name="file" type="file" class="form-control" id="inputEmail3" placeholder="file">
            </div>
          </div>
          
          <div class="form-group">
              <label for="sel1">Select start value(above start value):</label>
              <select name='start' class="form-control">
              @foreach($select as $key => $value)
                    <option value={{$key}}>{{$value}}</option>
              @endforeach
            </select>
          </div>
        
          <div class="form-group">
              <label for="sel1">Select end value(last value):</label>
              <select name='end' class="form-control">
              @foreach($select as $key => $value)
                    <option value={{$key}}>{{$value}}</option>
              @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-lg btn-outline-dark ">Submit</button>
            
        </form>
      @endif
      
      
</div>



<!-- Footer -->
<footer class="footer w3-center w3-black w3-padding-16">
  <p>Powered by <a href="/" title="Finance Report" target="_blank" class="w3-hover-text-green">Finance Report</a></p>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>
