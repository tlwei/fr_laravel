<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Spatie\PdfToText\Pdf;
use App\financeReport;


class DocxConversionRenew extends Controller
{
    //
    private $filename;
    private $title;
    private $start;
    private $end;
    
    public function __construct($filePath,$title,$start,$end) {
        $this->filename = $filePath;
        $this->title = $title;
        $this->start = $start;
        $this->end = $end;
    }

    public function convertToText() {
      
        if(isset($this->filename) && !file_exists($this->filename)) {
            return view('submit', ['return' => 'File Not exists!']);
        }

        $file_ext = $this->filename->getClientOriginalExtension();
            // if($file_ext == "docx") {
            //     return $this->read_docx();
            // }else 
            if($file_ext == "pdf") {
                return $this->read_pdf();
            }else if($file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "png") {
                return $this->read_img();
            }else {
                return view('submit', ['return' => 'Invalid File Type, accepted file type : docx/pdf/images']);
            }
    }

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    // t1
    // $start = Note RM’000 RM’000
    // $end = Cash and cash equivalents at 31 March B7 22,271 20,063

    //t2
    //start = RM’000 RM’000 RM’000 RM’000
    //end = - Diluted (sen) N/A N/A N/A N/A
    
    //t3
    //start = RM’000 =RM’000 RM’000 RM’000 RM’000 RM’000
    
    //end = At 31 March 2016 20,677 : 22,936 43,613 370 43,983

    public function read_img(){
        
        $toText = (new TesseractOCR($this->filename))
        ->run(); 
        $textToArr = explode("\n",$toText);
        // return $this->start;
        if($this->start == '' && $this->end == ''){
            return view('submit', ['return' => 'Select report start value and end value.','select' => $textToArr,'file'=>$this->filename,'title'=>$this->title]);
        }

        $start = $textToArr[$this->start];
        $end = $textToArr[$this->end];

        foreach ($textToArr as $key => $value) {
            if($value == $start){
                $startKey = $key;
            }else if($value == $end){
                $endKey = $key;
            }
        }
        if($startKey == null || $endKey == null){
            return view('submit', ['return' => "Error undefined start or end value",'select' => $textToArr,'file'=>$this->filename,'title'=>$this->title,'error'=>true]);
        }
        foreach ($textToArr as $key => $value) {
            if($key < $startKey){
                $getTable[] = [];
            }else if($key > $endKey){
                $getTable[] = [];
            }else{
                $getTable[] = $value;
            }
        }

        
        $removeEmpArr = array_values(array_filter($getTable));
        
        // return $removeEmpArr;
        foreach ($removeEmpArr as $key => $value) {
            if(preg_match('/[\d-]+/', $value) == 1){
                $deleteTitle[] = $value;
            }
        }
        
        foreach ($deleteTitle as $key => $value) {
            $explodeArr[] = explode(" ",$value);
        }

        $countHead = count($explodeArr[0]);

        foreach ($textToArr as $key => $value) {
            $getHeadValue[] = explode(" ",$value);
        }

        $count = count($getHeadValue[$startKey]);
        foreach ($getHeadValue[$startKey] as $key => $value) {
            $countHead = count($getHeadValue[$startKey])-1;
            $countSubHead = count($getHeadValue[$startKey-1])-1;
            $count--;
            if(isset($getHeadValue[$startKey][$countHead-$key]) && isset($getHeadValue[$startKey-1][$countSubHead-$key])){
                $mainHead[] = $count." ".$getHeadValue[$startKey-1][$countSubHead-$key]." ".$getHeadValue[$startKey][$countHead-$key];
            }else if($getHeadValue[$startKey][$countHead-$key] !== 'Note'){
                $mainHead[] = $count." ".$getHeadValue[$startKey][$countHead-$key];
            }
        }

        foreach($explodeArr as $key =>$value){
            if($key == 0){
                $getBody[] = [];
            }else{
                $getBody[] = $value;
            }
        }
        
        foreach($removeEmpArr as $key =>$value){
            if($key == 0 || preg_match('/[\d-]+/', $value) !== 1){
                $titleArr[] = [];
            }else{
                $titleArr[] = $value;
            }
        }
       
        $mainTitle = array_values(array_filter($titleArr));
        $mainBody = array_values(array_filter($getBody));
        
        //cincai
        
        if(count($mainHead)/2 !== 0){
            array_unshift($mainHead,'');
        }
        
        //
        foreach ($mainBody as $key => $value) {
            $countHead = count($mainHead)-1;
            $countBody = count($value);
            $count = 0;
            for ($i=count($mainHead)-2; $i >= 0; $i--) { 
                $count++;
                // $finalArr[$key][] = $i-1;
                // return $mainHead[$countHead+$i];
                if(isset($mainHead[$countHead-$i]) && isset($value[$countBody-$count])){
                    $finalArr[$key][$mainHead[$countHead-$i]] = $value[$countBody-$count];
                }else{
                    $finalArr[$key]=[];
                }
                
                // $mainTitle[$key] = str_replace($mainBody[$key][$countBody-$count], '', $mainTitle[$key]);
                
            }
            $finalArr[$key][""] = $mainTitle[$key];
        }
        foreach ($finalArr as $key => $value) {
            $finalArrR[$key] = array_reverse($value);
        }
        // return $mainTitle;
        financeReport::create([
            'title' => $this->title,
            'value' => json_encode($finalArrR),
            'file_type' => 'image'
        ]);
        return view('submit', ['return' => 'Data saved.','data' => $finalArrR]);
        return $finalArrR;
        
    }

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    public function read_pdf(){
    
            $string = null;
            $toText = (new Pdf())
            ->setPdf($this->filename)
            ->setOptions(['layout', 'r 96'])
            ->addOptions(['f 1'])
            ->text();

            $textToArr = explode("\n",$toText);
            
            foreach ($textToArr as $key => $value) {
                $string[$key] = str_replace('  ', '$', $value);
            }
           
            foreach ($string as $key => $value) {
                $explodeArr[$key] = explode("$",$value);
            }
            
            foreach ($explodeArr as $key => $value) {
                $removeEmp[$key] = array_values(array_filter($value));   
            }
            $removeEmpArr = array_values(array_filter($removeEmp));
            
            // return $removeEmpArr;
            foreach ($removeEmpArr as $key => $value) {
                $removeSpace[$key] = preg_replace('/ /', '', $value, 1);
            }
         
            foreach ($removeSpace as $key => $value) {
                if(count($value) == 1){
                    $removeArr[$key] = [];
                }else{
                    $removeArr[$key] = $value;
                }
            }
            
            $removeArrEmp = array_values(array_filter($removeArr));
            //t2 checkpoint
   
            
            //check head or body
            $headKey = null;
            foreach ($removeArrEmp as $key => $value) {
                $getCheckValue = count($value)-1;
                $remove1 = str_replace('(', '', $value[(int)$getCheckValue]);
                $remove2 = str_replace(')', '', $remove1);
                $remove3 = str_replace(',', '', $remove2);
                $remove4 = str_replace('.', '', $remove3);
               
                if ($remove4 == '-' || is_numeric($remove4) || $remove4 == 'N/A') { 
                    // $checkType[$key] = array_push($value,'body');
                    $checkType[$key] = $value;
                } else { 
                    // $checkType[$key] = array_push($value,'head');
                    $checkType[$key] = $value;
                    $headKey = $key;
                }
            }
            
            $splice = 0;
            if($headKey > 0){
                
                for ($i=$headKey;$i > -1; $i--) { 
                    $splice++;
                    $getBody = $i;
                    
                 }

                 array_splice($checkType,$getBody,$splice);
                 foreach ($checkType as $key => $value) {
                     if(count($checkType[0]) !== count($value) && count($checkType[0]) >= count($value)){
                        $counter = count($checkType[0]) - count($value);
                        for ($i=0; $i < $counter; $i++) { 
                            array_unshift($value,'');
                            $checkType[$key] = $value;
               
                        }
                     }
                 }
                 
            }else{
                //noheadfound
                financeReport::create([
                    'title' => $this->title,
                    'value' => json_encode($checkType),
                    'file_type' => 'pdf'
                ]);
                return $checkType;
            }
            $count = count($removeArrEmp[$headKey]);
            foreach($removeArrEmp[$headKey] as $hKey => $headValue){
                $count--;
                $countMainHead = count($removeArrEmp[$headKey])-1;
                $countSupHead = count($removeArrEmp[$headKey-1])-1;
                if(isset($removeArrEmp[$headKey-1]) && isset($removeArrEmp[$headKey-1][$countSupHead-$hKey]) ){
                    $mergeHead[] = $count.' '.$removeArrEmp[$headKey-1][$countSupHead-$hKey]." ".$removeArrEmp[$headKey][$countMainHead-$hKey];
                }else{
                    if($removeArrEmp[$headKey][$countMainHead-$hKey] !== 'Note'){
                        $mergeHead[] = $count.' '.$removeArrEmp[$headKey][$countMainHead-$hKey];
                    }
                }
            }
            
            //sperated
            array_push($mergeHead,'');
            $mainHead = array_reverse($mergeHead);
            $mainAllBody = $checkType;
  
            //combine head & body 
           
            foreach($mainAllBody as $bKey => $bValue){
                foreach ($mainHead as $hKey => $hValue) {

                    if($hValue == ""){
                        //title
                        $finalAll[$bKey][$hValue] = $bValue[$hKey];
                    }else{
                        //body
                        //$bValue[$hKey-1]
                        $filterHead = $mainHead;
                        $finalAll[$bKey][$hValue] = '';
                        
                    }
                    
                }
            }
            
            foreach($mainAllBody as $bKey => $bValue){
                // return count($bValue);
                $bodyCount = count($bValue);
                $headCount = count($filterHead);
                
                foreach ($mainHead as $hKey => $hValue) {
                    
                    if($hValue !== "" && $hValue !== "Note"){
                        //body
                        $finalAll[$bKey][$filterHead[$headCount-$hKey]] = $bValue[$bodyCount-$hKey];
                    }
                        
                }
                
            }

            financeReport::create([
                'title' => $this->title,
                'value' => json_encode($finalAll),
                'file_type' => 'pdf'
            ]);
            return view('submit', ['return' => 'Data saved.','data' => $finalAll]);
            return $finalAll;
    }


    //////////
    //////////
    //////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    ///////////
    ///////////
    ///////////
    ///////////
    ///////////
    ///////////

    // private function read_docx(){
        
    //     $striped_content = '';
    //     $content = '';

    //     $zip = zip_open($this->filename);
        
    //     if (!$zip || is_numeric($zip)) return false;
        
    //     while ($zip_entry = zip_read($zip)) {
    //         if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
    //         if (zip_entry_name($zip_entry) != "word/document.xml") continue;

    //         $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

    //         zip_entry_close($zip_entry);
    //     }// end while
        
    //     zip_close($zip);
    //     //horizontal
    //     $content = str_replace('</w:r></w:p></w:tc><w:tc>', "insideArr", $content);
    //     //vertical
    //     $content = str_replace('</w:r></w:p>', "toArr", $content);

    //     $striped_content = strip_tags($content);
    
    //     $toArr = explode("toArr", $striped_content);
        
    //     //all array
    //     $removeEmp = array_values(array_filter(array_map('trim', $toArr), 'strlen'));
        
    //     $arrToArr = [];
    //     foreach ($removeEmp as $key => $value) {
    //         $arrToArr[] = explode("insideArr", $value);
    //     }
        
    //     //filter only data
    //     $head = null;
        
        
       
    //     //head > $arrToArr[$head]
    //     foreach ($arrToArr as $key => $value) {
    //         if(count($value) == 1){
    //             $arrToArr[$key] = [];
    //         }
    //     }
    //     $filterArr = array_values(array_filter($arrToArr));
    //     return $filterArr;
    //     //merge data
    //     foreach ($filterArr as $key => $value) {
    //         foreach ($value as $data_key => $data) {
    //             if($filterArr[$key][$data_key] !== ''){
    //                 $finalArr[$key][$filterArr[0][$data_key]]=$filterArr[$key][$data_key];   
    //             }
                
    //         }
    //     }

    //     $finalArrFilter = array_values(array_filter($finalArr));

    //     financeReport::create([
    //         'title' => $title[0],
    //         'value' => json_encode($finalArrFilter),
    //         'file_type' => 'docx'
    //     ]);

    //     // return "Saved Report data to Database"."\n\n"."File Type: docx"."\n\n".json_encode($finalArrFilter);
    //     return $finalArrFilter;
    // }

}
