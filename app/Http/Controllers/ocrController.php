<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use thiagoalessio\TesseractOCR\TesseractOCR;

class ocrController extends Controller
{

    public function getFile(Request $request){
        
        $validate = Validator::make($request->all(), [
            'file'=>'required',
        ])->validate();
        
        $docObj = new DocxConversion($validate['file']);
        $docText= $docObj->convertToText();

        return $docText;
    }

    public function getFileRenew(Request $request){
        $start = '';
        $end = '';
        $validate = Validator::make($request->all(), [
            'file'=>'required',
            'title'=>'required',
            'start'=>'',
            'end'=>'',
        ])->validate();
     
        if(isset($validate['start']) && isset($validate['end'])){
            $start = $validate['start'];
            $end = $validate['end'];
        }
        $docObj = new DocxConversionRenew($validate['file'],$validate['title'],$start,$end);
        $docText= $docObj->convertToText();
        
        return $docText;
    }
    
}
